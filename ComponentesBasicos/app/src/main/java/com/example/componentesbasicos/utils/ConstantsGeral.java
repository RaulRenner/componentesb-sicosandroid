package com.example.componentesbasicos.utils;

public class ConstantsGeral {

    //SOMA CLICKS
    public static String SOMA_CLICKS = "SOMA: ";

    //DADOS ALUNOS
    public static String NOME = "NOME";
    public static String SOBRENOME = "SOBRENOME";
    public static String MATRICULA = "MATRICULA";
    public static String CURSO = "CURSO";
    public static String QUALIDADE_LABORATORIO = "QUALIDADE_LABORATORIO";
    public static String UTILIZACAO_LABORATORIO = "UTILIZACAO_LABORATORIO";
    public static String DISPONIBILIDADE_LABORATORIO = "DISPONIBILIDADE_LABORATORIO";

    //CODIGO DE NAVEGACAO DE TELAS
    public static int REQUEST_COD = 18;
    public static int RESULT_COD = 20;
    public static int REQUEST_COD_EDIT = 19;
    public static int RESULT_CANCELAR = 17;

    //Mensagens
    public static String ITEM_SELECIONADO = "Item selecionado";
    public static String ALUNO_CADASTRADO = "Aluno cadastrado com sucesso";
    public static String ITEM_NAO_SELECIONADO = "Nenhum item selecionado";
    public static String ALUNO_EDITADO = "Dados do aluno atualizado";
    public static String ALUNO_REMOVIDO = "Aluno removido com sucesso";
    public static String ACAO_CANCELADA = "Ação cancelada!";



}
