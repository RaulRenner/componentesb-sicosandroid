package com.example.componentesbasicos.model;

import android.os.Parcelable;

import java.io.Serializable;

public class Aluno {
    private String nome;
    private String sobrenome;
    private String matricula;
    private String curso;

    private PesquisaLaboratorio pesquisaLaboratorio;

//    private String turno_de_utilizacao;
////    private String qualidade_laboratorio;
////    private String disponibilidade_laboratorio;

    public Aluno(String nome, String sobrenome, String matricula, String curso, PesquisaLaboratorio pesquisaLaboratorio) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.matricula = matricula;
        this.curso = curso;
        this.pesquisaLaboratorio = pesquisaLaboratorio;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public PesquisaLaboratorio getPesquisaLaboratorio() {
        return pesquisaLaboratorio;
    }

    public void setPesquisaLaboratorio(PesquisaLaboratorio pesquisaLaboratorio) {
        this.pesquisaLaboratorio = pesquisaLaboratorio;
    }


    @Override
    public String toString() {
        return
                "nome:" + nome + '\n' +
                "sobrenome: " + sobrenome + '\n' +
                "matricula: " + matricula + '\n' +
                "curso: " + curso + '\n' +
                pesquisaLaboratorio;
    }

}
