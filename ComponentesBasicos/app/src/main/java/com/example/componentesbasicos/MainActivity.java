package com.example.componentesbasicos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.componentesbasicos.model.Aluno;
import com.example.componentesbasicos.utils.ConstantsGeral;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, PopupMenu.OnMenuItemClickListener {

   private  AutoCompleteTextView auto_complete_curso;
   private Spinner spinner_turno;
   private RadioGroup radioGroup_qld_lab;
   private RadioButton rad_btn_qld_lab;
   private RadioGroup radioGroup_disp_lab;
   private RadioButton radioButton_disp_lab;
   Aluno aluno;
   private EditText nome;
   private EditText sobrenome;
   private EditText matricula;
   private Button btn_cadastrar_aluno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        inicializarCampos();

        AutoComplete();
        spinnerTurno();

        editarDados();
        adicionarAluno();

    }

    public void inicializarCampos(){
        nome = (EditText) findViewById(R.id.editText_nome);
        sobrenome = (EditText) findViewById(R.id.editText_sobrenome);
        matricula = ( EditText ) findViewById(R.id.editText_matricula);
        radioGroup_disp_lab = (RadioGroup) findViewById(R.id.rad_gp_disp_lab);
        radioGroup_qld_lab = (RadioGroup) findViewById(R.id.rad_group_qld_lab);
        auto_complete_curso = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
    }

    public void spinnerTurno(){
        spinner_turno = (Spinner) findViewById(R.id.spinner_turno);

        ArrayAdapter adapter_spinner_turno =
                ArrayAdapter.createFromResource(this,R.array.turno,android.R.layout.simple_spinner_item);
        adapter_spinner_turno.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_turno.setOnItemSelectedListener(this);
        spinner_turno.setAdapter(adapter_spinner_turno);

    }
    public void AutoComplete(){
        ArrayAdapter<String> adapter_auto_complete =
                new ArrayAdapter<>(this,android.R.layout.simple_dropdown_item_1line,cursos);
        auto_complete_curso.setThreshold(1);
        auto_complete_curso.setAdapter(adapter_auto_complete);
    }

    private static final String[] cursos = new String[]{
    "Ciencia da Computacao","Designer Digital","Engenharia da Computacao","Engenharia de Software"
            ,"Redes de Computadores","Sistema de Informação"
    };

    public void editarDados(){

        if(getIntent().getExtras() != null){
            nome.setText( (String) getIntent().getExtras().get( ConstantsGeral.NOME ) );
            sobrenome.setText( (String) getIntent().getExtras().get( ConstantsGeral.SOBRENOME ) );
            matricula.setText( (String) getIntent().getExtras().get( ConstantsGeral.MATRICULA ) );
            matricula.setEnabled( false );
        }

        //Bundle bundle =
//        if(bundle != null && (bundle.containsKey( "EDIT_DADOS" ))){
//            aluno = (Aluno) bundle.getSerializable( "EDIT_DADOS" );
//            nome.setText( aluno.getNome() );
//            sobrenome.setText( aluno.getSobrenome() );
//            matricula.setText( aluno.getMatricula() );
//            matricula.setEnabled( false );
//        }


    }

    public void adicionarAluno(){

        btn_cadastrar_aluno = (Button) findViewById(R.id.btn_cadastrar_aluno);

       btn_cadastrar_aluno.setOnClickListener(new View.OnClickListener() {
            @Override
           public void onClick(View v) {

                int id_rad_group_qld_lab = radioGroup_qld_lab.getCheckedRadioButtonId();
                rad_btn_qld_lab =  findViewById(id_rad_group_qld_lab);

                int id_radio_group_disp_lab = radioGroup_disp_lab.getCheckedRadioButtonId();
                radioButton_disp_lab = (RadioButton) findViewById(id_radio_group_disp_lab);

               // Toast.makeText(MainActivity.this,spinner_turno.getSelectedItem().toString(),Toast.LENGTH_SHORT).show();

               Intent intent = new Intent();
                intent.putExtra( ConstantsGeral.NOME, nome.getText().toString());
                intent.putExtra(ConstantsGeral.SOBRENOME, sobrenome.getText().toString());
                intent.putExtra(ConstantsGeral.MATRICULA, matricula.getText().toString());
                intent.putExtra(ConstantsGeral.CURSO,  auto_complete_curso.getText().toString());
                intent.putExtra(ConstantsGeral.UTILIZACAO_LABORATORIO, spinner_turno.getSelectedItem().toString());
                intent.putExtra(ConstantsGeral.QUALIDADE_LABORATORIO, rad_btn_qld_lab.getText().toString());
                intent.putExtra(ConstantsGeral.DISPONIBILIDADE_LABORATORIO, radioButton_disp_lab.getText().toString());

                setResult( ConstantsGeral.RESULT_COD, intent );
                finish();
            }
        });
   }
   public void cancelar( View view ){
        setResult( ConstantsGeral.RESULT_CANCELAR);
        finish();
   }
   public void limparDados( View view  ){
       PopupMenu popupMenu = new PopupMenu( this, view );
       popupMenu.setOnMenuItemClickListener( this );
       popupMenu.inflate( R.menu.menu_drop_down_popup_menu );
       popupMenu.show();
   }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        this.spinner_turno.getSelectedItem().toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.limpar_campos:
            nome.setText( "" );
            sobrenome.setText( "" );
            matricula.setText( "" );
            auto_complete_curso.setText( "" );
            radioGroup_disp_lab.clearCheck();
            radioGroup_qld_lab.clearCheck();


            return true;

        }
        return false;
    }
}
