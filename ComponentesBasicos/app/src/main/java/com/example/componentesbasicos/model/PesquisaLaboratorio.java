package com.example.componentesbasicos.model;

public class PesquisaLaboratorio {

    private String turno_de_utilizacao;
    private String qualidade_laboratorio;
    private String disponibilidade_laboratorio;

    public PesquisaLaboratorio(String turno_de_utilizacao, String qualidade_laboratorio, String disponibilidade_laboratorio) {
        this.turno_de_utilizacao = turno_de_utilizacao;
        this.qualidade_laboratorio = qualidade_laboratorio;
        this.disponibilidade_laboratorio = disponibilidade_laboratorio;
    }

    public PesquisaLaboratorio() {
    }

    public String getTurno_de_utilizacao() {
        return turno_de_utilizacao;
    }

    public void setTurno_de_utilizacao(String turno_de_utilizacao) {
        this.turno_de_utilizacao = turno_de_utilizacao;
    }

    public String getQualidade_laboratorio() {
        return qualidade_laboratorio;
    }

    public void setQualidade_laboratorio(String qualidade_laboratorio) {
        this.qualidade_laboratorio = qualidade_laboratorio;
    }

    public String getDisponibilidade_laboratorio() {
        return disponibilidade_laboratorio;
    }

    public void setDisponibilidade_laboratorio(String disponibilidade_laboratorio) {
        this.disponibilidade_laboratorio = disponibilidade_laboratorio;
    }

    @Override
    public String toString() {
        return
                "turno_de_utilizacao: " + turno_de_utilizacao + '\n' +
                "qualidade_laboratorio: " + qualidade_laboratorio + '\n' +
                "disponibilidade_laboratorio: " + disponibilidade_laboratorio + '\n' ;
    }
}
