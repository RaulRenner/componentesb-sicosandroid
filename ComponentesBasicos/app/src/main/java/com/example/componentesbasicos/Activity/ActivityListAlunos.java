package com.example.componentesbasicos.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.componentesbasicos.MainActivity;
import com.example.componentesbasicos.R;
import com.example.componentesbasicos.model.Aluno;
import com.example.componentesbasicos.model.PesquisaLaboratorio;
import com.example.componentesbasicos.utils.ConstantsGeral;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class ActivityListAlunos extends AppCompatActivity {
    private FloatingActionButton fab_add;
    private FloatingActionButton fab_next_activity;
    private ListView listView_alunos;
    private Aluno aluno;
    private PesquisaLaboratorio pesquisaLaboratorio;
    private List<Aluno>alunos;
    private ArrayAdapter<Aluno>alunoArrayAdapter;
    private int selected = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_list_alunos );

        iniciarComponents();
        adicionarAluno();
        proxima_Activity();
        selecionarItemListView();
    }


    public void iniciarComponents(){
        alunos = new ArrayList<>(  );
        fab_add = findViewById( R.id.fab_add_aluno );
        fab_next_activity = findViewById( R.id.fab_proxima_activit );
        listView_alunos = (ListView) findViewById( R.id.list_dados_alunos );
        alunoArrayAdapter = new ArrayAdapter<>( ActivityListAlunos.this, android.R.layout.simple_list_item_1, alunos );
        listView_alunos.setAdapter( alunoArrayAdapter );

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult( requestCode, resultCode, data );

        if(requestCode == ConstantsGeral.REQUEST_COD && resultCode == ConstantsGeral.RESULT_COD){
            String nome = data.getStringExtra( ConstantsGeral.NOME );
            String sobrenome = data.getStringExtra(ConstantsGeral.SOBRENOME);
            String matricula = data.getStringExtra(ConstantsGeral.MATRICULA);
            String curso = data.getStringExtra(ConstantsGeral.CURSO);
            String utilizacao_laboratorio = data.getStringExtra(ConstantsGeral.UTILIZACAO_LABORATORIO);
            String qualidade_laboratorio = data.getStringExtra(ConstantsGeral.QUALIDADE_LABORATORIO);
            String disponibilidade_laboratorio = data.getStringExtra(ConstantsGeral.DISPONIBILIDADE_LABORATORIO);

            pesquisaLaboratorio = new PesquisaLaboratorio( utilizacao_laboratorio, qualidade_laboratorio, disponibilidade_laboratorio );
            aluno = new Aluno( nome, sobrenome, matricula, curso, pesquisaLaboratorio );

            alunos.add( aluno );
            alunoArrayAdapter.notifyDataSetChanged();

            mensagem( ConstantsGeral.ALUNO_CADASTRADO );

        }else if(requestCode == ConstantsGeral.REQUEST_COD_EDIT && resultCode == ConstantsGeral.RESULT_COD ){
            String nome = (String) data.getExtras().get( ConstantsGeral.NOME );
            String sobrenome = (String) data.getExtras().get(ConstantsGeral.SOBRENOME);
            String matricula = (String) data.getExtras().get(ConstantsGeral.MATRICULA);
            String curso = (String) data.getExtras().get(ConstantsGeral.CURSO);
            String utilizacao_laboratorio = (String) data.getExtras().get(ConstantsGeral.UTILIZACAO_LABORATORIO);
            String qualidade_laboratorio = (String) data.getExtras().get(ConstantsGeral.QUALIDADE_LABORATORIO);
            String disponibilidade_laboratorio = (String) data.getExtras().get(ConstantsGeral.DISPONIBILIDADE_LABORATORIO);

            for(Aluno alunoEdit : alunos){
                if(aluno.getMatricula().equals( matricula )){
                    pesquisaLaboratorio = new PesquisaLaboratorio( utilizacao_laboratorio, qualidade_laboratorio, disponibilidade_laboratorio );
                    alunoEdit.setNome( nome );
                    alunoEdit.setSobrenome( sobrenome );
                    alunoEdit.setCurso( curso );
                    alunoEdit.setPesquisaLaboratorio( pesquisaLaboratorio );

                }
            }

            alunoArrayAdapter.notifyDataSetChanged();
            mensagem( ConstantsGeral.ALUNO_EDITADO );

        }else if(resultCode == ConstantsGeral.RESULT_CANCELAR){
            mensagem( ConstantsGeral.ACAO_CANCELADA );
        }
    }

    public void adicionarAluno(){
        fab_add.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( ActivityListAlunos.this, MainActivity.class );
                startActivityForResult( intent, ConstantsGeral.REQUEST_COD );
            }
        } );
    }

    public void selecionarItemListView(){

        listView_alunos.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    mensagem( ConstantsGeral.ITEM_SELECIONADO );
                    selected = position;
            }
        } );
    }
    public void proxima_Activity(){
        fab_next_activity.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( ActivityListAlunos.this, GridViewImagens.class );
                startActivity( intent );

            }
        } );
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate( R.menu.menu_crud, menu );
        return super.onCreateOptionsMenu( menu );
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.editar:
                editar();
                break;
            case R.id.delete:
                remover();
                break;

        }
        return true;
    }

    public void mensagem(String mensagem){
        Toast.makeText( ActivityListAlunos.this, mensagem, Toast.LENGTH_SHORT ).show();
    }
    public void editar(){
        if(selected >= 0){
            Intent intent = new Intent( ActivityListAlunos.this, MainActivity.class );
            intent.putExtra( ConstantsGeral.NOME, alunos.get( selected ).getNome() );
            intent.putExtra( ConstantsGeral.SOBRENOME, alunos.get( selected ).getSobrenome() );
            intent.putExtra(ConstantsGeral.MATRICULA, alunos.get( selected ).getMatricula() );
            intent.putExtra( ConstantsGeral.CURSO, alunos.get( selected ).getCurso() );

//            Bundle bundle = new Bundle(  );
//
//
//            bundle.putString( ConstantsGeral.NOME, alunos.get( selected ).getNome());
//            bundle.putString( ConstantsGeral.SOBRENOME, alunos.get( selected ).getSobrenome() );
//            bundle.putString( ConstantsGeral.MATRICULA, alunos.get( selected ).getMatricula() );
//            bundle.putString( ConstantsGeral.CURSO, alunos.get( selected ).getCurso() );
//
//
//            intent.putExtras(bundle);
            startActivityForResult( intent, ConstantsGeral.REQUEST_COD_EDIT );
        }else{
            mensagem( ConstantsGeral.ITEM_NAO_SELECIONADO );
        }
    }

    public void remover(){
        if(selected >= 0){
            String matricula_selecionada = alunos.get( selected ).getMatricula();
            for(int i = 0; i < alunos.size(); i++){
                if(matricula_selecionada.equals( alunos.get( i ).getMatricula() )){
                    alunos.remove( i );
                }
            }
            alunoArrayAdapter.notifyDataSetChanged();
            mensagem( ConstantsGeral.ALUNO_REMOVIDO );
        }else{
            mensagem( ConstantsGeral.ITEM_NAO_SELECIONADO );
        }
    }
}