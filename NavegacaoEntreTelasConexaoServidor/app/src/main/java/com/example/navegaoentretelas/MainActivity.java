package com.example.navegaoentretelas;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.navegaoentretelas.model.Constants;
import com.example.navegaoentretelas.model.Medico;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Medico> medicos = new ArrayList<Medico>(  );
    private Medico medico;
    private String crm_Medico;
    private ListView lista_view_medicos;
    private ArrayAdapter adapter_medico;
    private EditText edit_CRM_medico;
    private Button btn_editar;
    private int selected;

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
        selected = -1;



        instanciarListViewMedicos();
        selecionarItemListView();
        editarDadosMedico();

        inicializarFirebase();
        eventsDatabase();
    }

    public void cadastrarMedico( View view ){
        Intent intent = new Intent( this, ActivityCadastramento.class );
        startActivityForResult( intent, Constants.REQUEST_ADD );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        medico = new Medico(  );
        super.onActivityResult( requestCode, resultCode, data );

        if( requestCode == Constants.REQUEST_ADD && resultCode == Constants.RESULT_ADD){
            medico.setNome_completo( data.getStringExtra( "NOME_COMPLETO" ) );
            medico.setEspecialidade( data.getStringExtra( "ESPECIALIZACAO" ));
            medico.setAreaAtuacao( data.getStringExtra( "AREA ATUACAO" ) );
            medico.setCRM( data.getStringExtra( "CRM" ));

            databaseReference.child( "Medico" ).child( medico.getCRM() ).setValue( medico );
           // medicos.add( medico );
            adapter_medico.notifyDataSetChanged();

        }else if(requestCode == Constants.REQUEST_EDIT && resultCode == Constants.RESULT_ADD){
                //medicos.remove( selected );
                String nome = (String) data.getExtras().get( "NOME_COMPLETO" );
                String especialidade = (String) data.getExtras().get( "ESPECIALIZACAO" );
                String area_atuacao = (String) data.getExtras().get( "AREA ATUACAO" ) ;
                String crm = (String) data.getExtras().get( "CRM" );

            for(Medico medico : medicos){
                if (medico.getCRM().equals( crm )){
                    medico = new Medico( nome, especialidade, area_atuacao, crm );
                    databaseReference.child( "Medico" ).child( medico.getCRM() ).setValue( medico );
                }
            }
                //medicos.add( medico );
                adapter_medico.notifyDataSetChanged();
        }else if (resultCode == Constants.RESULT_CANCEL){
            Toast.makeText( MainActivity.this, "Cancelado", Toast.LENGTH_LONG ).show();
        }
        edit_CRM_medico.setText( "" );
        edit_CRM_medico.setEnabled( true );
        selected = -1;
    }

    public void instanciarListViewMedicos(){
        lista_view_medicos = (ListView) findViewById( R.id.list_view_medicos) ;
        edit_CRM_medico = (EditText) findViewById( R.id.editText_CRM_medico);

        //adapter_medico = new ArrayAdapter<>( this, android.R.layout.simple_list_item_1, medicos );
        lista_view_medicos.setSelection( android.R.color.holo_green_dark );
        //lista_view_medicos.setAdapter( adapter_medico );

    }

    public void eventsDatabase(){
        databaseReference.child( "Medico" ).addValueEventListener( new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                medicos.clear();

                for (DataSnapshot objectSnapshot : dataSnapshot.getChildren()) {
                    Medico medico = objectSnapshot.getValue( Medico.class );
                    medicos.add( medico );

                }
                adapter_medico = new ArrayAdapter<Medico>( MainActivity.this, android.R.layout.simple_list_item_1, medicos );
                lista_view_medicos.setAdapter( adapter_medico );



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void selecionarItemListView(){
        edit_CRM_medico = (EditText) findViewById( R.id.editar_CRM_medico);
        lista_view_medicos.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText( MainActivity.this, "Item selecionado", Toast.LENGTH_LONG ).show();
                selected = position;
                edit_CRM_medico.setText( medicos.get( selected ).getCRM() );
                edit_CRM_medico.setEnabled( false );

            }
        } );
    }

    public void editarDadosMedico(){

        btn_editar = (Button) findViewById( R.id.btn_editar_dados_medicos );
//
        btn_editar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selected >= 0){
                    Intent intent = new Intent( MainActivity.this, ActivityCadastramento.class );
                    intent.putExtra( "NOME_COMPLETO", medicos.get( selected ).getNome_completo() );
                    intent.putExtra( "ESPECIALIZACAO", medicos.get( selected ).getEspecialidade() );
                    intent.putExtra( "AREA ATUACAO", medicos.get( selected ).getAreaAtuacao() );
                    intent.putExtra( "CRM" , medicos.get( selected ).getCRM());

                    startActivityForResult(intent,Constants.REQUEST_EDIT);
                }else if(edit_CRM_medico != null && verificarMedico( edit_CRM_medico.getText().toString() )){
                    selected = getCRM_medico( edit_CRM_medico.getText().toString() );
                    Intent intent = new Intent( MainActivity.this, ActivityCadastramento.class );
                    intent.putExtra( "NOME_COMPLETO", medicos.get( selected ).getNome_completo() );
                    intent.putExtra( "ESPECIALIZACAO", medicos.get( selected ).getEspecialidade() );
                    intent.putExtra( "AREA ATUACAO", medicos.get( selected ).getAreaAtuacao() );
                    intent.putExtra( "CRM" , medicos.get( selected ).getCRM());

                    startActivityForResult(intent,Constants.REQUEST_EDIT);
                }else{
                    Toast.makeText( MainActivity.this, "Nenhum item encontrado", Toast.LENGTH_LONG ).show();
                }

            }
        } );


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate( R.menu.menu_buttons, menu );
        return super.onCreateOptionsMenu( menu );
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_button_rmv:
                removerItem();
                edit_CRM_medico.setText( "" );
                edit_CRM_medico.setEnabled( false );
                break;
        }
        selected = -1;
        return true;

    }

    public void removerItem() {
        if (selected >= 0) {
            String crm_medico = medicos.get( selected ).getCRM();
            medico = new Medico();
            medico.setCRM( crm_medico );
            databaseReference.child( "Medico" ).child( medico.getCRM() ).removeValue();
            Toast.makeText( MainActivity.this, "Medico Removido com sucesso!", Toast.LENGTH_SHORT ).show();
            adapter_medico.notifyDataSetChanged();
        }else{
            Toast.makeText( MainActivity.this, "Nenhum item selecionado", Toast.LENGTH_SHORT ).show();
        }
    }

    public boolean verificarMedico(String crm){
        for(int i = 0; i < medicos.size(); i++){
            if(crm.equals( medicos.get( i ).getCRM() )){
                return true;
            }
        }
        return false;

    }

    public int getCRM_medico(String crm){
        for(int i = 0; i < medicos.size(); i++) {
            if (crm.equals( medicos.get( i ).getCRM() )) {
                return i;
            }
        }
        return 0;
    }
//    public void onClickListView(){
//        Medico medico2 = new Medico(  );
//        lista_view_medicos.setOnItemClickListener( new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//              edit_crm_medico.setText(medicos.get( position ).getCRM());
//
//            }
//        });0
//    }
    public void inicializarFirebase(){
        FirebaseApp.initializeApp( MainActivity.this );
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseDatabase.setPersistenceEnabled( true );
        databaseReference = firebaseDatabase.getReference();
    }
}
