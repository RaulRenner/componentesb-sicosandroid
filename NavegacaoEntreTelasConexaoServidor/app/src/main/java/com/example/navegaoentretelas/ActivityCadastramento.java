package com.example.navegaoentretelas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.navegaoentretelas.model.Constants;

import java.util.Random;

public class ActivityCadastramento extends AppCompatActivity {
    private EditText edit_nome;
    private EditText edit_especialidade;
    private EditText edt_AreaAtuacao;
    private EditText edt_CRM;
    private Button btn_salvar;
    private Random random;
    public Boolean flagEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_cadastramento );

        inicializarComponentes();
        recebendoDados();
        cadastrarMedico();
    }

    public void inicializarComponentes(){
        edit_nome = (EditText) findViewById( R.id.edit_nome_medico );
        edit_especialidade = (EditText) findViewById( R.id.edit_especialidade);
        edt_AreaAtuacao = (EditText) findViewById( R.id.edit_area_atuacao);
        edt_CRM = (EditText) findViewById( R.id.editText_CRM_medico);
        edt_CRM.setEnabled( false );
    }
    public void recebendoDados() {



        if(getIntent().getExtras() != null){
            edit_nome.setText( (String) getIntent().getExtras().get( "NOME_COMPLETO" ) );
            edit_especialidade.setText( (String) getIntent().getExtras().get( "ESPECIALIZACAO" ) );
            edt_AreaAtuacao.setText( (String) getIntent().getExtras().get( "AREA ATUACAO" ));
            edt_CRM.setText( (String) getIntent().getExtras().get( "CRM" ));
            flagEdit = true;
        }

    }

    public void cadastrarMedico(){
        btn_salvar = (Button) findViewById( R.id.btn_salvar_dados_medico );
        final String crm;
        if(!flagEdit){
            crm = Integer.toString( gerarCRM() );
            edt_CRM.setText( crm );
        }else {
            crm = edt_CRM.getText().toString();
        }
        btn_salvar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();

                if(verificacarCampos( edit_nome.getText().toString(), edit_especialidade.getText().toString(),
                        edt_AreaAtuacao.getText().toString() )){
                    intent.putExtra( "NOME_COMPLETO", edit_nome.getText().toString() );
                    intent.putExtra( "ESPECIALIZACAO",edit_especialidade.getText().toString());
                    intent.putExtra( "AREA ATUACAO", edt_AreaAtuacao.getText().toString() );

                    intent.putExtra( "CRM", crm);
                    setResult( Constants.RESULT_ADD, intent );
                    finish();
                }else{
                    Toast.makeText( ActivityCadastramento.this, "Campos vazios", Toast.LENGTH_LONG ).show();
                }



            }
        } );
    }
    public void cancelar(View view ){
        setResult( Constants.RESULT_CANCEL );
        finish();
    }
    public int gerarCRM(){
        random = new Random(  );
        int crm = random.nextInt(80000);

        return  crm;
    }

    public  boolean verificacarCampos(String nome, String especializacao, String area_atuacao){
        if(nome.equals( "" ) || especializacao.equals( "" ) || area_atuacao.equals( "" )){
            return false;
        }
        return true;
    }

}
