package com.example.businfinancontrol.Activity.empreendedor.perfil.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.view.CadReservaCliente;
import com.example.businfinancontrol.R;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

public class PageListReservasClientesFragment extends Fragment {
    private ExtendedFloatingActionButton extendedFloatingActionButton;
    private TextView lista_vazia;
    private ExpandableListView expandableListView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate( R.layout.page_reservas_clientes_fragment_body, container, false );
        extendedFloatingActionButton = (ExtendedFloatingActionButton) view.findViewById( R.id.btn_cad_reserva );
        lista_vazia = (TextView) view.findViewById( R.id.text_lista_vazia );
        expandableListView = view.findViewById( R.id.expandableListView );

        extendedFloatingActionButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getActivity(), CadReservaCliente.class );
                startActivity( intent );
            }
        } );
        verificacaoListaVazia();
        return view;
    }

    public void verificacaoListaVazia(){
//        if(balancoMensalAdapter.getItemCount() == 0){
        lista_vazia.setVisibility( View.VISIBLE );
//        }else{
//            lista_vazia.setVisibility( View.GONE );
//        }
    }
}
