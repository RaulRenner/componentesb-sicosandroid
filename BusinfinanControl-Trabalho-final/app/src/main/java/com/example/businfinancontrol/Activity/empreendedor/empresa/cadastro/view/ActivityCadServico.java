package com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.businfinancontrol.Activity.utils.Constants;
import com.example.businfinancontrol.R;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ActivityCadServico extends AppCompatActivity {

    private Button btn_voltar, btn_cadastrar;
    private EditText edt_nome_servico, edt_valor_servico, edt_descricao_servico;
    private FloatingActionButton btn_delete;
    private int id_servico;
    private boolean result = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_cad_servico );

        getSupportActionBar().setDisplayShowHomeEnabled( true );
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        inicializarComponentes();
        eventosClickButton();
        eventEditDadosServicos();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    public void inicializarComponentes(){
        btn_cadastrar = (Button) findViewById( R.id.btn_add );
        btn_voltar = (Button) findViewById( R.id.btn_voltar );
        edt_nome_servico = (EditText) findViewById( R.id.nome_cliente );
        edt_valor_servico = (EditText) findViewById( R.id.valorTotalServico );
        edt_descricao_servico = (EditText) findViewById( R.id.horario_reserva );
        btn_delete = (FloatingActionButton) findViewById( R.id.btnf_delete );
        btn_delete.setVisibility( View.INVISIBLE );

    }



    public void eventosClickButton(){
        btn_voltar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_cadastrar.setOnClickListener( new View.OnClickListener() {
            double valor_servico;
            @Override
            public void onClick(View v) {
                if(!validarCampos( edt_nome_servico.getText().toString(), edt_valor_servico.getText().toString(),
                        edt_descricao_servico.getText().toString())){
                        valor_servico = Float.valueOf( edt_valor_servico.getText().toString() );

                        Intent intent = new Intent(  );

                        intent.putExtra( Constants.NOME_SERVICO, edt_nome_servico.getText().toString() );
                        intent.putExtra( Constants.VALOR_SERVICO, valor_servico );
                        intent.putExtra( Constants.DESCRICAO_SERVICO, edt_descricao_servico.getText().toString() );

                        if (result == true){
                            intent.putExtra( Constants.ID_SERVICO, id_servico );
                        }
                        setResult( Constants.RESULT_ADD, intent);
                        finish();


                }
            }
        } );
        if(btn_cadastrar.getText().toString().equals( "Salvar modificações" )){
            btn_cadastrar.setText("Cadastrar");
        }

        btn_delete.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder( ActivityCadServico.this );
                builder.setTitle( "Excluir Serviço" );
                builder.setMessage( "Deseja realmente remover esse serviço?" );
                builder.setPositiveButton( "SIM", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(id_servico > 0 ){
                            Intent intent = new Intent(  );
                            intent.putExtra( Constants.ID_SERVICO, id_servico );
                            setResult( Constants.RESULT_RMV, intent );
                            finish();
                        }else{
                            Toast.makeText( getApplicationContext(), "Nenhum servico selecionado para remover", Toast.LENGTH_LONG ).show();
                        }
                    }
                } );
                builder.setNegativeButton( "NÃO",  null);
                builder.show();

                }
            });
        }

    public boolean validarCampos( String nome_servico, String valor_servico, String descricao_servico){
        boolean flag = false;

        if(flag = isCamposVazios( nome_servico )){
           edt_nome_servico.requestFocus();
        }else if (flag = isCamposVazios( descricao_servico )){
            edt_descricao_servico.requestFocus();
        }else if (flag = isCamposVazios( valor_servico )){
            edt_valor_servico.requestFocus();
        }

        if(flag){
            AlertDialog.Builder camposInvalidos = new AlertDialog.Builder( this );
            camposInvalidos.setTitle( "Aviso" );
            camposInvalidos.setMessage( "Há campos inválidos ou em branco." );
            camposInvalidos.setNeutralButton( "OK", null );
            camposInvalidos.show();
        }

        return flag;

    }

    public boolean isCamposVazios( String campo){

        boolean resultado =(TextUtils.isEmpty( campo ) || campo.trim().isEmpty());

        return resultado;
    }

    public void eventEditDadosServicos(){
        if(getIntent().getExtras() != null){
            btn_delete.setVisibility( View.VISIBLE );
            btn_cadastrar.setText( "Salvar modificações" );
            edt_nome_servico.setText( (String) getIntent().getExtras().get( Constants.NOME_SERVICO ) );
            edt_valor_servico.setText( String.valueOf(getIntent().getExtras().get( Constants.VALOR_SERVICO )) );
            edt_descricao_servico.setText((String) getIntent().getExtras().get( Constants.DESCRICAO_SERVICO ));
            id_servico = (int) getIntent().getExtras().get( Constants.ID_SERVICO );

            result = true;

        }
    }


}