package com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.model.Servico;
import com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.view.ActivityCadServico;
import com.example.businfinancontrol.Activity.utils.Constants;
import com.example.businfinancontrol.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class ServicoListAdapter extends RecyclerView.Adapter<ServicoListAdapter.ViewHolderServico> {

    private List<Servico> servicoList;
    private Context context;
    private int selected = -1;



    public ServicoListAdapter(Context context){
        this.context = context;
        servicoList = new ArrayList<Servico>( );
    }


    @NonNull
    @Override
    public ViewHolderServico onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from( parent.getContext() );


        View view = layoutInflater.inflate( R.layout.layout_linha_dados_servicos_list,  parent, false);

        ViewHolderServico holderServicoList = new ViewHolderServico( view, parent.getContext());
        return holderServicoList;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderServico holder, final int position) {

        if(servicoList != null && servicoList.size() > 0){
            Servico servico = servicoList.get( position );
            holder.text_nome_servico.setText( servico.getNomeServico() );
            holder.text_valor_servico.setText( String.valueOf(servico.getValor()) );
            holder.text_desricao_servico.setText( servico.getDescricaoServico() );

        }

//        holder.btn_edit.setOnClickListener( new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(selected >= 0){
//                    Intent intent = new Intent( context, ActivityCadServico.class );
//                    intent.putExtra( Constants.NOME_SERVICO, servicoList.get( selected ).getNomeServico() );
//                    intent.putExtra( Constants.VALOR_SERVICO, String.valueOf( servicoList.get( selected ).getValor() ) );
//                    intent.putExtra( Constants.DESCRICAO_SERVICO, servicoList.get( selected ).getDescricaoServico() );
//                    context.startActivity( intent );
//                }
//
//            }
//        } );
            holder.linearLayout_item.setOnClickListener( new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Toast.makeText( context, "Item selecionado", Toast.LENGTH_SHORT ).show();
                    Intent intent = new Intent( context, ActivityCadServico.class );
                        intent.putExtra( Constants.NOME_SERVICO, servicoList.get( position ).getNomeServico() );
                        intent.putExtra( Constants.VALOR_SERVICO, String.valueOf( servicoList.get( position ).getValor() ) );
                        intent.putExtra( Constants.DESCRICAO_SERVICO, servicoList.get( position ).getDescricaoServico() );
                        intent.putExtra( Constants.ID_SERVICO, servicoList.get( position ).getId() );
                        ((AppCompatActivity)context).startActivityForResult( intent, Constants.REQUEST_EDIT );

                }
            } );

    }

    @Override
    public int getItemCount() {
        return servicoList.size();
    }
    public class ViewHolderServico extends RecyclerView.ViewHolder{
        public TextView text_nome_servico;
        public TextView text_valor_servico;
        public TextView text_desricao_servico;
        public LinearLayout linearLayout_item;


        public ViewHolderServico(@NonNull View itemView, final Context context) {
            super( itemView );

            text_nome_servico = (TextView) itemView.findViewById( R.id.cont_nome_servico );
            text_valor_servico = (TextView) itemView.findViewById( R.id.text_valor_servico );
            text_desricao_servico = (TextView) itemView.findViewById( R.id.text_descricao_servico );
            linearLayout_item =  (LinearLayout) itemView.findViewById( R.id.linear_item_dados_servico );




        }
    }
    public void setServicosList(List<Servico> servicosList){
        this.servicoList = servicosList;
        notifyDataSetChanged();
    }

    public List<Servico> getListServicos(){
        return servicoList;
    }
}
