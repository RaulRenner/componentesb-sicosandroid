package com.example.businfinancontrol.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.businfinancontrol.Activity.empreendedor.perfil.MainActivity;
import com.example.businfinancontrol.Activity.empreendedor.perfil.fragments.PerfilEmpreendedorFragments;
import com.example.businfinancontrol.R;

public class ActivityLogin extends AppCompatActivity {
    private Button button_login;
    private TextView btn_text_cad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_login );

        inicializarComponentes();
        eventosButton();
        eventLinkClickEntrar();

    }

//    public void abrirTelaCadastrar(View view ){
//        Intent intent = new Intent (getApplicationContext(), CadastroEmpreendedor.class);
//        startActivity( intent );
//    }

    public void inicializarComponentes(){

        button_login = (Button) findViewById( R.id.btn_login );
        btn_text_cad = (TextView) findViewById( R.id.text_cad );
    }
    public void eventosButton(){
        button_login.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( ActivityLogin.this, MainActivity.class );
                startActivity( intent );
                finish();
            }
        } );
    }

    public void eventLinkClickEntrar(){
        SpannableString textViewClickEntrar = new SpannableString( btn_text_cad.getText().toString() );
        textViewClickEntrar.setSpan( new CustumLinkEntrar(), 0,11, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );

        btn_text_cad.setText( textViewClickEntrar );
        btn_text_cad.setMovementMethod( LinkMovementMethod.getInstance() );
    }

    class CustumLinkEntrar extends ClickableSpan {

        @Override
        public void onClick(@NonNull  View widget) {
            Intent intent = new Intent( getApplicationContext(), CadastroEmpreendedor.class );
            startActivity( intent );

        }
        @Override
        public void updateDrawState(@NonNull TextPaint ds) {
            ds.setColor( Color.BLUE );
            ds.setUnderlineText( false );

        }
    }

}