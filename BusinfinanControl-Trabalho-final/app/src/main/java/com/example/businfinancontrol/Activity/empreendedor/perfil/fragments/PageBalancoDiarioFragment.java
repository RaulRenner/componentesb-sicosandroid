package com.example.businfinancontrol.Activity.empreendedor.perfil.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.businfinancontrol.Activity.ActivityLogin;
import com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.view.ActivityBalancoMensal;
import com.example.businfinancontrol.R;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

public class PageBalancoDiarioFragment extends Fragment {

    private ExtendedFloatingActionButton extendedFloatingActionButton;
    private TextView lista_vazia;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view  = inflater.inflate( R.layout.page_balanco_diario_fragment_body, container ,  false);

        extendedFloatingActionButton = (ExtendedFloatingActionButton) view.findViewById( R.id.btn_cad_reserva );
        lista_vazia = (TextView) view.findViewById( R.id.text_lista_vazia );

        extendedFloatingActionButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder( getActivity() );
                builder.setTitle( "Encerrar Diária" );
                builder.setMessage( "Deseja realmente encerrar diária?" );
                builder.setPositiveButton( "SIM", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent( getActivity(), ActivityBalancoMensal.class );
                        startActivity( intent );
                    }
                } );
                builder.setNegativeButton( "NÃO",  null);
                builder.show();
            }
        } );
        verificacaoListaVazia();
        return view;
    }

    public void verificacaoListaVazia(){
//        if(balancoMensalAdapter.getItemCount() == 0){
            lista_vazia.setVisibility( View.VISIBLE );
//        }else{
//            lista_vazia.setVisibility( View.GONE );
//        }
        }
}
