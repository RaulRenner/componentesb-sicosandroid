package com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.viewModel;

import android.widget.ListView;

import com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.model.Servico;

import java.util.List;

public interface IServicoObserver {
    public void atualizarListaServicos(List<Servico> list_servicos);
}
