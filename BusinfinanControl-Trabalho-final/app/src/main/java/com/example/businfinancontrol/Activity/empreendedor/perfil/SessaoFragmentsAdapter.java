package com.example.businfinancontrol.Activity.empreendedor.perfil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class SessaoFragmentsAdapter extends FragmentPagerAdapter {
    private List<Fragment> fragmentList = new ArrayList<>(  );
    private List<String> fragmentStringList = new ArrayList<>(  );
    public SessaoFragmentsAdapter( FragmentManager fm) {
        super( fm );
    }

    public void adicionarFragment( Fragment fragment, String titulo_fragment){
            fragmentList.add( fragment );
            fragmentStringList.add( titulo_fragment );
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragmentList.get( position );
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentStringList.get( position );
    }
}
