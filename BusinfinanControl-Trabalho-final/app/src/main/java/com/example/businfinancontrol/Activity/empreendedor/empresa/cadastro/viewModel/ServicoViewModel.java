package com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.viewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.model.Servico;

import java.util.List;

public class ServicoViewModel extends ViewModel implements IServicoObserver{
    public MutableLiveData<List<Servico>> listMutableLiveData;

    public void inicializacaoConfigRepositorio(){}

    public LiveData<List<Servico>> getServicos(){
        if (listMutableLiveData == null){
            listMutableLiveData = new MutableLiveData<>();
        }

        return listMutableLiveData;
    }

    public void adicionarNovoServico(String nome_servico, float valor_servico, String descricao_servico){

    }

    public void getDadosListRepositorio(){

    }
    @Override
    public void atualizarListaServicos(List<Servico> list_servicos) {
        if(listMutableLiveData != null){
            listMutableLiveData.postValue( list_servicos );
        }
    }
}
