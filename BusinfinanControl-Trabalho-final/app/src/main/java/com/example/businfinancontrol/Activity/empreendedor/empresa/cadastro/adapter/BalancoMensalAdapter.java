package com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.businfinancontrol.Activity.empreendedor.empresa.model.BalanceamentoMensal;
import com.example.businfinancontrol.R;

import java.util.ArrayList;
import java.util.List;

public class BalancoMensalAdapter extends RecyclerView.Adapter<BalancoMensalAdapter.ViewHolderMensalAdapt> {
    private List<BalanceamentoMensal> balanceamentoMensalList;
    private Context context;

    public BalancoMensalAdapter(Context context) {
        this.balanceamentoMensalList = new ArrayList<BalanceamentoMensal>(  );
        this.context = context;
    }


    @NonNull
    @Override
    public BalancoMensalAdapter.ViewHolderMensalAdapt onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from( parent.getContext() );
        View view  = layoutInflater.inflate( R.layout.layout_dados_list_balanco_mensal, parent, false );

        ViewHolderMensalAdapt holderviewHolderMensalAdapt = new ViewHolderMensalAdapt( view, parent.getContext() );
        return holderviewHolderMensalAdapt;
    }

    @Override
    public void onBindViewHolder(@NonNull BalancoMensalAdapter.ViewHolderMensalAdapt holder, int position) {

        if(balanceamentoMensalList != null && balanceamentoMensalList.size() > 0){
            BalanceamentoMensal balanceamentoMensal = balanceamentoMensalList.get( position );
            holder.text_data_servico.setText( balanceamentoMensal.getData() );
            holder.text_qtd_clientes.setText( String.valueOf( balanceamentoMensal.getNumero_total_clientes()));
            holder.text_valor_total.setText( String.valueOf( balanceamentoMensal.getValor_total() ));
        }

    }


    @Override
    public int getItemCount() {
        return balanceamentoMensalList.size();
    }

    public void setListBalancoMensal(List<BalanceamentoMensal> balanceamentoMensalList){
        this.balanceamentoMensalList = balanceamentoMensalList;
        notifyDataSetChanged();
    }

    public class ViewHolderMensalAdapt extends RecyclerView.ViewHolder {
        public TextView text_data_servico;
        public TextView text_qtd_clientes;
        public TextView text_valor_total;
        public ViewHolderMensalAdapt(@NonNull View itemView, final Context context) {
            super( itemView );

            text_data_servico = (TextView) itemView.findViewById( R.id.text_data_servico );
            text_qtd_clientes = (TextView) itemView.findViewById( R.id.qtd_total_clientes );
            text_valor_total = (TextView) itemView.findViewById( R.id.valor_total );


        }
    }
}
