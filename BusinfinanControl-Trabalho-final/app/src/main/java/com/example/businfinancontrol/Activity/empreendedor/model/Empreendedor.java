package com.example.businfinancontrol.Activity.empreendedor.model;

import com.example.businfinancontrol.Activity.empreendedor.empresa.model.Empresa;
import com.example.businfinancontrol.entity.Endereco;
import com.example.businfinancontrol.entity.Usuario;

import java.util.Date;

public class Empreendedor extends Usuario {
    private Empresa empresa;

    public Empreendedor(String nome, String sobrenome, String telefone, String apelido, Date data_nascimento,
                        String email, String senha, Empresa empresa) {
        super( nome, sobrenome, telefone, apelido, data_nascimento,email, senha );
        this.empresa = empresa;
    }

    public Empreendedor() {
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
}
