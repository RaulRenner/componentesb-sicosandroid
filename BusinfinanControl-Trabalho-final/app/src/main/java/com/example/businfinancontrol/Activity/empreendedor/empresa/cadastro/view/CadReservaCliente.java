package com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.adapter.ServicoListAdapter;
import com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.model.Servico;
import com.example.businfinancontrol.Activity.empreendedor.perfil.fragments.PageListReservasClientesFragment;
import com.example.businfinancontrol.Activity.utils.Constants;
import com.example.businfinancontrol.R;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.ArrayList;
import java.util.List;

public class CadReservaCliente extends AppCompatActivity {

    private List<Servico> servicos;
    private ServicoListAdapter servicoListAdapter;
    private String[] listNomeServicos;
    private boolean[] checkdItens;
    private ArrayList<Integer> mPosItens ;

    private Button btnSelecionarServico;
    private TextView textViewValorTotalServico;
    private Button btnCadastrar;

    Servico sv1;
    Servico sv2;
    Servico sv3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_cad_reserva_cliente );

        getSupportActionBar().setDisplayShowHomeEnabled( true );
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        sv1 = new Servico( "Corte Social","Corte cabelo normal",13 );
        sv2 = new Servico( "Corte Degrade", "Corte Degrade na maquina", 20 );
        sv3 = new Servico( "Corte Degrade tesoura", "Degrade todo na tesoura", 25 );


        inicializarComponentesGeral();
        inicializarComponentesListaServicos();
        eventClickButtonSelcionarServico();
        eventClicksButton();

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    public void voltar(View view ){
        finish();
    }
    public void inicializarComponentesGeral(){
        servicos = new ArrayList<>(  );
        mPosItens = new ArrayList<>(  );
        servicoListAdapter = new ServicoListAdapter( this );

        //apenas para teste
        servicos.add( sv1 );
        servicos.add( sv2 );
        servicos.add( sv3 );

        servicoListAdapter.setServicosList( servicos );

        //servicos = servicoListAdapter.getListServicos();
        btnSelecionarServico = (Button) findViewById( R.id.btn_selecionar_servico );
        textViewValorTotalServico = (TextView) findViewById( R.id.valorTotalServico );
        btnCadastrar = (Button) findViewById( R.id.btn_add );
    }
    public void inicializarComponentesListaServicos(){

        checkdItens = new boolean[servicoListAdapter.getItemCount()];
        listNomeServicos = new String[servicoListAdapter.getItemCount()];

        for(int i = 0; i < servicos.size(); i++){
            listNomeServicos[i] = servicos.get( i ).getNomeServico();
        }

    }

    public void eventClickButtonSelcionarServico(){
        btnSelecionarServico.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder mBuilder = new AlertDialog.Builder( CadReservaCliente.this );
                mBuilder.setTitle( "Serviços" );
                mBuilder.setMultiChoiceItems(listNomeServicos, checkdItens, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int pos, boolean isChecked) {
                        if(isChecked){
                            if(!mPosItens.contains( pos )){
                                mPosItens.add( pos );
                            }
                        }else if(mPosItens.contains( pos )){
                            mPosItens.remove( pos );
                        }

                    }
                } );

                mBuilder.setCancelable( false );
                mBuilder.setPositiveButton( "Confirmar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        double valor = 0;

                        for(int i = 0; i < mPosItens.size(); i++){
                            valor += servicos.get( mPosItens.get( i ) ).getValor();
                        }
                        textViewValorTotalServico.setText( String.valueOf(  valor ) );
                    }

                } );
                mBuilder.setNegativeButton( "Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                } );
                mBuilder.setNeutralButton( "Clear all", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        for( int i = 0; i < checkdItens.length; i++){
                            checkdItens[i] = false;
                            mPosItens.clear();
                            textViewValorTotalServico.setText( "Valor total servico" );
                        }
                    }
                } );

                AlertDialog alertDialog = mBuilder.create();
                alertDialog.show();
            }
        } );
    }
    public void eventClicksButton(){
        btnCadastrar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder( CadReservaCliente.this );
                builder.setTitle( "Cadastrar Reserva" );
                builder.setMessage( "Deseja realmente finalizar a reserva do cliente ?" );
                builder.setPositiveButton( "SIM", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent( CadReservaCliente.this, PageListReservasClientesFragment.class );
                        startActivity( intent );
                    }
                } );
                builder.setNegativeButton( "NÃO",  null);
                builder.show();
            }
        } );
    }

}