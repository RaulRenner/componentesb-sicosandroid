package com.example.businfinancontrol.Activity.empreendedor.perfil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;

import com.example.businfinancontrol.Activity.empreendedor.perfil.fragments.PageBalancoDiarioFragment;
import com.example.businfinancontrol.Activity.empreendedor.perfil.fragments.PageListReservasClientesFragment;
import com.example.businfinancontrol.Activity.empreendedor.perfil.fragments.PerfilEmpreendedorFragments;
import com.example.businfinancontrol.R;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private SessaoFragmentsAdapter sessaoFragmentsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        final DrawerLayout drawerLayout = findViewById( R.id.main_perfil_empreendedor );
        findViewById( R.id.imageMenu ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer( GravityCompat.START );
            }
        } );
        NavigationView navigationView = findViewById( R.id.navigation_drawer );
        navigationView.setItemIconTintList( null );


        sessaoFragmentsAdapter =new SessaoFragmentsAdapter( getSupportFragmentManager() );
        viewPager =(ViewPager) findViewById( R.id.body_page );
        setupViewPageFragments( viewPager );
        TabLayout tabLayout = (TabLayout) findViewById( R.id.tabs );
        tabLayout.setupWithViewPager( viewPager );


    }


    public void setupViewPageFragments(ViewPager viewPager){
        SessaoFragmentsAdapter sessaoFragmentsAdapter = new SessaoFragmentsAdapter( getSupportFragmentManager() );

       sessaoFragmentsAdapter.adicionarFragment( new PerfilEmpreendedorFragments(), "Perfil" );
       sessaoFragmentsAdapter.adicionarFragment( new PageListReservasClientesFragment(), "Clientes" );
       sessaoFragmentsAdapter.adicionarFragment( new PageBalancoDiarioFragment() , "Balanço Diário" );

       viewPager.setAdapter( sessaoFragmentsAdapter );

    }
}