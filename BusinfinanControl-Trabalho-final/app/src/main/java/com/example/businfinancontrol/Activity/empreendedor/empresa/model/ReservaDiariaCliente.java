package com.example.businfinancontrol.Activity.empreendedor.empresa.model;

import java.util.Date;
import java.util.Timer;

public class ReservaDiariaCliente {
    // pode-se ocorrer uma mudança para um objeto do tipo Cliente;
    private String cliente;
    private String horario;
    private String dataReserva;
    private boolean status;

    public ReservaDiariaCliente(String cliente, String horario, String dataReserva) {
        this.cliente = cliente;
        this.horario = horario;
        this.dataReserva = dataReserva;
        this.status = status;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getDataReserva() {
        return dataReserva;
    }

    public void setDataReserva(String dataReserva) {
        this.dataReserva = dataReserva;
    }
}
