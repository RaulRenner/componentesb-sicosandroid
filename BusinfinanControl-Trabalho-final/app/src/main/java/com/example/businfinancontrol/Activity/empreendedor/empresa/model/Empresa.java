package com.example.businfinancontrol.Activity.empreendedor.empresa.model;

import com.example.businfinancontrol.entity.Endereco;

public class Empresa {

    private String nomeEmpresa;
    private String cnpj;
    private String tipoEstabelecimento;
    private String descricaoEstabelecimento;
    private Endereco endereco;

    public Empresa(String nome_empresa, String cnpj, String tipo_estabelecimento, String descricao_estabelecimento, Endereco endereco) {
        this.nomeEmpresa = nome_empresa;
        this.cnpj = cnpj;
        this.tipoEstabelecimento = tipo_estabelecimento;
        this.descricaoEstabelecimento = descricao_estabelecimento;
        this.endereco = endereco;
    }

    public Empresa() {
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getTipoEstabelecimento() {
        return tipoEstabelecimento;
    }

    public void setTipoEstabelecimento(String tipoEstabelecimento) {
        this.tipoEstabelecimento = tipoEstabelecimento;
    }

    public String getDescricaoEstabelecimento() {
        return descricaoEstabelecimento;
    }

    public void setDescricaoEstabelecimento(String descricaoEstabelecimento) {
        this.descricaoEstabelecimento = descricaoEstabelecimento;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
}
