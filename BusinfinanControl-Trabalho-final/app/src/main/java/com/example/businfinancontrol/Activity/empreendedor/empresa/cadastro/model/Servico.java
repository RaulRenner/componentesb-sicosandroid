package com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.model;

public class Servico {
    private int id_generator = 1;
    private String nomeServico;
    private String descricaoServico;
    private double valor;
    private int id;

    public Servico(String nome_Servico, String descricao_servico, double valor) {
        this.nomeServico = nome_Servico;
        this.descricaoServico = descricao_servico;
        this.valor = valor;
        this.id = id_generator++;
    }

    public Servico() {
    }

    public String getNomeServico() {
        return nomeServico;
    }

    public void setNomeServico(String nomeServico) {
        this.nomeServico = nomeServico;
    }

    public String getDescricaoServico() {
        return descricaoServico;
    }

    public void setDescricaoServico(String descricaoServico) {
        this.descricaoServico = descricaoServico;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
