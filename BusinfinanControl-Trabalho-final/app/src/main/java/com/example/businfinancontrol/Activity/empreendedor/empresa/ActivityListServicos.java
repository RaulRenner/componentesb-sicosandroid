package com.example.businfinancontrol.Activity.empreendedor.empresa;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.adapter.ServicoListAdapter;
import com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.model.Servico;
import com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.view.ActivityCadServico;
import com.example.businfinancontrol.Activity.utils.Constants;
import com.example.businfinancontrol.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class ActivityListServicos extends AppCompatActivity {

    private FloatingActionButton floatButtonAdd;
    private RecyclerView recyclerView_list_servicos;
    private ServicoListAdapter servicoListAdapter;

    private String nome_servico;
    private String descricao;
    private double valor;
    private TextView lista_vazia;

    //sofrerá mudanças
    private List<Servico> servicoList;
    private Servico servico;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_list_servicos );

        getSupportActionBar().setDisplayHomeAsUpEnabled( true );
        getSupportActionBar().setDisplayShowHomeEnabled( true );

        inicializarComponentes();
        eventosButton();
        verificacaoListaVazia();

    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }


    public void inicializarComponentes() {
        floatButtonAdd = (FloatingActionButton) findViewById( R.id.btn_add );
        lista_vazia = (TextView) findViewById( R.id.text_lista_vazia );

        servicoList = new ArrayList<Servico>();
        servicoListAdapter = new ServicoListAdapter( this );

        recyclerView_list_servicos = (RecyclerView) findViewById( R.id.list_recycle_servicos );
        recyclerView_list_servicos.setHasFixedSize( true );
        recyclerView_list_servicos.setLayoutManager( new LinearLayoutManager( getApplicationContext() ) );
        recyclerView_list_servicos.setAdapter( servicoListAdapter );


    }

    public void eventosButton() {
        floatButtonAdd.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getApplicationContext(), ActivityCadServico.class );
                startActivityForResult( intent, Constants.REQUEST_ADD );
            }
        } );
    }


    //sofrerá modificações
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult( requestCode, resultCode, data );
        if (requestCode == Constants.REQUEST_ADD && resultCode == Constants.RESULT_ADD) {
            nome_servico = (String) data.getExtras().get( Constants.NOME_SERVICO );
            descricao = (String) data.getExtras().get( Constants.DESCRICAO_SERVICO );
            valor =  (Double) data.getExtras().get( Constants.VALOR_SERVICO );

            servico = new Servico( nome_servico, descricao, valor );
            servicoList.add( servico );
            mensagemToast( Constants.mensagem_cadastro_servico );
            servicoListAdapter.notifyDataSetChanged();
            servicoListAdapter.setServicosList( servicoList );
            verificacaoListaVazia();

        } else if (requestCode == Constants.REQUEST_EDIT && resultCode == Constants.RESULT_ADD) {
            nome_servico = (String) data.getExtras().get( Constants.NOME_SERVICO );
            descricao = (String) data.getExtras().get( Constants.DESCRICAO_SERVICO );
            valor =  (Double) data.getExtras().get( Constants.VALOR_SERVICO );
            int id_servico = (int) Integer.valueOf( (int) data.getExtras().get( Constants.ID_SERVICO ) );

            for (Servico servico : servicoList) {
                if (servico.getId() == id_servico) {
                    servico.setNomeServico( nome_servico );
                    servico.setDescricaoServico( descricao );
                    servico.setValor( valor );
                }
            }
            mensagemToast( Constants.mensagem_editar_servico );
            servicoListAdapter.setServicosList( servicoList );
            verificacaoListaVazia();

        } else if (resultCode == Constants.RESULT_RMV) {
            int id_cliente = (int) data.getExtras().get( Constants.ID_SERVICO );
            for (int i = 0; i < servicoList.size(); i++) {
                if (servicoList.get( i ).getId() == id_cliente) {
                    servicoList.remove( i );
                    break;
                }
            }
            mensagemToast( Constants.mensagem_remocao_servico );
            servicoListAdapter.setServicosList( servicoList );
            verificacaoListaVazia();

        }
    }
        public void mensagemToast (String mensagem){
            Toast.makeText( ActivityListServicos.this, mensagem, Toast.LENGTH_LONG ).show();
        }

        public void verificacaoListaVazia(){
            if(servicoListAdapter.getItemCount() == 0){
                lista_vazia.setVisibility( View.VISIBLE );
            }else{
                lista_vazia.setVisibility( View.GONE );
            }
        }
}