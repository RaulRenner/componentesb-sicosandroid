package com.example.businfinancontrol.Activity.empreendedor.empresa.model;

import java.util.Date;

public class BalanceamentoDiario {

    private double valorTotal;
    private int numeroTotalClientes;
    private Date data;

    public BalanceamentoDiario(double valorTotal, int numeroTotalClientes, Date data) {
        this.valorTotal = valorTotal;
        this.numeroTotalClientes = numeroTotalClientes;
        this.data = data;
    }

    public BalanceamentoDiario() {
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public int getNumeroTotalClientes() {
        return numeroTotalClientes;
    }

    public void setNumeroTotalClientes(int numeroTotalClientes) {
        this.numeroTotalClientes = numeroTotalClientes;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
