package com.example.businfinancontrol.Activity.empreendedor.perfil.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.businfinancontrol.Activity.ActivityLogin;
import com.example.businfinancontrol.Activity.empreendedor.empresa.ActivityListServicos;
import com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.view.ActivityBalancoMensal;
import com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.view.ActivityMaps;
import com.example.businfinancontrol.R;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class PerfilEmpreendedorFragments extends Fragment {

    private Button btn_servico;
    private Button btn_financeiro;
    private Button btn_sair;
    private Button btn_localizacao;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view  = inflater.inflate(R.layout.page_perfil_emp_fragment_body, container, false);

        btn_servico = (Button) view.findViewById( R.id.btn_serviços );
        btn_financeiro = (Button) view.findViewById( R.id.btn_balanco_mensal );
        btn_sair = (Button) view.findViewById( R.id.btn_logout );
        btn_localizacao = (Button) view.findViewById( R.id.btn_localizacao );


        btn_servico.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getActivity(), ActivityListServicos.class );
                startActivity( intent );

            }
        } );

        btn_financeiro.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getActivity(), ActivityBalancoMensal.class );
                startActivity( intent );
            }
        } );

        btn_sair.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder( getActivity() );
                builder.setTitle( "Sair da conta" );
                builder.setMessage( "Deseja realmente sair da conta?" );
                builder.setPositiveButton( "SIM", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                     Intent intent = new Intent( getActivity(), ActivityLogin.class );
                     startActivity( intent );
                    }
                } );
                builder.setNegativeButton( "NÃO",  null);
                builder.show();
            }
        } );

        btn_localizacao.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getActivity(), ActivityMaps.class );
                startActivity( intent );
            }
        } );

        return view;
    }
}
