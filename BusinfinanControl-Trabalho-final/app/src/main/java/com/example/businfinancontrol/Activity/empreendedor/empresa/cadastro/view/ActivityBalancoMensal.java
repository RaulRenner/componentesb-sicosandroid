package com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.adapter.BalancoMensalAdapter;
import com.example.businfinancontrol.Activity.empreendedor.empresa.model.BalanceamentoMensal;
import com.example.businfinancontrol.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ActivityBalancoMensal extends AppCompatActivity {
    private RecyclerView recyclerView;
    private List<BalanceamentoMensal>balanceamentoMensalList;
    private BalancoMensalAdapter balancoMensalAdapter;
    private TextView lista_vazia;
    BalanceamentoMensal bl = new BalanceamentoMensal("123",20, 100);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_balanco_mensal );

        getSupportActionBar().setDisplayShowHomeEnabled( true );
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        inicializarComponentes();
        verificacaoListaVazia();
    }

    public void inicializarComponentes(){
        balanceamentoMensalList = new ArrayList<BalanceamentoMensal>(  );
        lista_vazia = (TextView) findViewById( R.id.text_lista_vazia );

        recyclerView = (RecyclerView) findViewById( R.id.recycle_balanco_mensal );
        balancoMensalAdapter = new BalancoMensalAdapter( this );
        recyclerView.setHasFixedSize( true );
        recyclerView.setLayoutManager( new LinearLayoutManager( getApplicationContext() ) );
        recyclerView.setAdapter( balancoMensalAdapter );
    }





    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    public void verificacaoListaVazia(){
        if(balancoMensalAdapter.getItemCount() == 0){
            lista_vazia.setVisibility( View.VISIBLE );
        }else{
            lista_vazia.setVisibility( View.GONE );
        }
    }
}