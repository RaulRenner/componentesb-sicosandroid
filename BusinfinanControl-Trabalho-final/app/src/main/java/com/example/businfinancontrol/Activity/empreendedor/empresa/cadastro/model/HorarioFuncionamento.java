package com.example.businfinancontrol.Activity.empreendedor.empresa.cadastro.model;

public class HorarioFuncionamento {
    private String horarioInicio;
    private String horarioEncerramento;
    private String dia;
    //data

    public HorarioFuncionamento(String horario_inicio, String horario_encerramento, String dia) {
        this.horarioInicio = horario_inicio;
        this.horarioEncerramento = horario_encerramento;
        this.dia = dia;
    }

    public HorarioFuncionamento() {
    }

    public String getHorarioInicio() {
        return horarioInicio;
    }

    public void setHorarioInicio(String horarioInicio) {
        this.horarioInicio = horarioInicio;
    }

    public String getHorarioEncerramento() {
        return horarioEncerramento;
    }

    public void setHorarioEncerramento(String horarioEncerramento) {
        this.horarioEncerramento = horarioEncerramento;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }
}
