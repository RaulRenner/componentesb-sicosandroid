package com.example.businfinancontrol.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.businfinancontrol.R;
import com.google.android.material.textfield.TextInputEditText;

public class CadastroEmpreendedor extends AppCompatActivity {

    private EditText editText_nome;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_cadastro_empreendedor );

        editText_nome = (EditText) findViewById( R.id.edit_nome_empreendedor );
        button = (Button) findViewById( R.id.btn_add );
        button.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText( getApplicationContext(), editText_nome.getText().toString(), Toast.LENGTH_LONG ).show();
            }
        } );
    }

    public void voltarTelaLogin(View view){
        Intent intent = new Intent( getApplicationContext(), ActivityLogin.class );
        startActivity( intent );
    }
}