package com.example.businfinancontrol.Activity.empreendedor.empresa.model;

import java.util.Date;

public class BalanceamentoMensal {
    private static int id_generator = 1;
    private String data;
    private int numero_total_clientes;
    private double valor_total;
    private int id;

    public BalanceamentoMensal(String data, int numero_total_clientes, double valor_total) {
        this.data = data;
        this.numero_total_clientes = numero_total_clientes;
        this.valor_total = valor_total;
        id = id_generator++;
    }

    public BalanceamentoMensal() {
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getNumero_total_clientes() {
        return numero_total_clientes;
    }

    public void setNumero_total_clientes(int numero_total_clientes) {
        this.numero_total_clientes = numero_total_clientes;
    }

    public double getValor_total() {
        return valor_total;
    }

    public void setValor_total(double valor_total) {
        this.valor_total = valor_total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
