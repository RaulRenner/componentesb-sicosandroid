package com.example.uolthasapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class ActivityLogin extends AppCompatActivity {

    private TextView txtLinkCadastro;
    private Button btnLogin;
    private EditText editTextEmail;
    private EditText editTextSenha;

    private String email;
    private String senha;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_login );

        init();
        eventsClicks();
    }

    public void init(){
        txtLinkCadastro = (TextView) findViewById( R.id.textViewCadastro );
        btnLogin = (Button) findViewById( R.id.btn_login );
        editTextEmail = (EditText) findViewById( R.id.edt_email );
        editTextSenha = (EditText) findViewById( R.id.edt_senha );
    }

    public void eventsClicks(){

        txtLinkCadastro.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( ActivityLogin.this, ActivityCadastro.class );
                startActivity( intent );
            }
        } );

        btnLogin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = editTextEmail.getText().toString();
                senha = editTextSenha.getText().toString();

                Log.i("Teste", email);
                Log.i( "Teste", senha );
                if(email == null || email.isEmpty() || senha == null  || senha.isEmpty()){
                    Toast.makeText( ActivityLogin.this, "Preencha todos os campos", Toast.LENGTH_LONG ).show();
                    return;
                }

                FirebaseAuth.getInstance().signInWithEmailAndPassword( email, senha )
                        .addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()){
                                    Log.i("Teste2", task.getResult().getUser().getUid());

                                    Intent intent = new Intent( ActivityLogin.this, ActivityMensagens.class );
                                    intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );

                                    startActivity( intent );

                                }
                            }
                        } )
                        .addOnFailureListener( new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.i("Teste2", e.getMessage());

                            }
                        } );


                }
            } );

        }
    }