package com.example.uolthasapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.uolthasapp.models.User;
import com.example.uolthasapp.utils.Constants;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;
import com.xwray.groupie.GroupAdapter;
import com.xwray.groupie.Item;
import com.xwray.groupie.OnItemClickListener;
import com.xwray.groupie.ViewHolder;

import java.util.List;

public class ActivityContatos extends AppCompatActivity {
    private RecyclerView recyclerViewContatos;
    private GroupAdapter groupAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_contatos );
        init();
        buscarUsuariosCadastrados();
    }




    public void init(){
        recyclerViewContatos = findViewById( R.id.recycle_ViewContatos );
        groupAdapter = new GroupAdapter();
        recyclerViewContatos.setAdapter( groupAdapter );
        recyclerViewContatos.setLayoutManager( new LinearLayoutManager( this ) );
        groupAdapter.setOnItemClickListener( new OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull Item item, @NonNull View view) {
                Intent intent = new Intent (ActivityContatos.this, ChatActivity.class);
                UserItem userItem =  (UserItem ) item;

                intent.putExtra( Constants.USER, userItem.user );
                startActivity( intent );
            }
        } );

    }



    public void buscarUsuariosCadastrados(){
        FirebaseFirestore.getInstance().collection( Constants.USERS )
                .addSnapshotListener( new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null){
                            Log.i("Teste", error.getMessage(), error);
                            return;
                        }

                        List<DocumentSnapshot> docs = value.getDocuments();
                        groupAdapter.clear();
                        for(DocumentSnapshot doc : docs){
                            User user = doc.toObject( User.class );
                            String uid = FirebaseAuth.getInstance().getUid();
                            if (user.getUuid().equals(uid))
                                continue;
                            groupAdapter.add( new UserItem( user ) );
                            groupAdapter.notifyDataSetChanged();
                        }
                    }
                } );
    }
    private class UserItem extends Item<ViewHolder> {
        private final User user;

        private UserItem(User user) {
            this.user = user;
        }


        @Override
        public void bind(@NonNull ViewHolder viewHolder, int position) {
            TextView nomeUser  = viewHolder.itemView.findViewById( R.id.textNomeContato );
            ImageView imgContato = viewHolder.itemView.findViewById( R.id.image_contato );

            nomeUser.setText( user.getUsername() );
            Picasso.get().load( user.getUriImg() )
                    .into( imgContato );
        }

        @Override
        public int getLayout() {
            return R.layout.item_user_layout;
        }
    }
}