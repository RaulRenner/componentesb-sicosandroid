package com.example.uolthasapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.uolthasapp.models.Contato;
import com.example.uolthasapp.models.Mensagem;
import com.example.uolthasapp.models.User;
import com.example.uolthasapp.utils.Constants;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;
import com.xwray.groupie.GroupAdapter;
import com.xwray.groupie.Item;
import com.xwray.groupie.ViewHolder;

import java.util.List;

public class ChatActivity extends AppCompatActivity {

    private GroupAdapter groupAdapter;
    private RecyclerView recyclerViewChatMessagens;
    private User user;
    private User userAtual;
    private Button btnSendMesg;
    private EditText editTextMsgChat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_chat );
            user = getIntent().getExtras().getParcelable( Constants.USER );
            getSupportActionBar().setTitle( user.getUsername() );

        recyclerViewChatMessagens = (RecyclerView) findViewById( R.id.recycleViewChatMsg );

        btnSendMesg = (Button) findViewById( R.id.button_send );
        editTextMsgChat = (EditText) findViewById( R.id.editTextTextPersonName );

        btnSendMesg.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviarMessagem();
            }
        } );


        groupAdapter = new GroupAdapter();
        recyclerViewChatMessagens.setLayoutManager( new LinearLayoutManager( this ) );
        recyclerViewChatMessagens.setAdapter( groupAdapter );

        FirebaseFirestore.getInstance().collection( Constants.USERS )
                .document(FirebaseAuth.getInstance().getUid())
                .get()
                .addOnSuccessListener( new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        userAtual = documentSnapshot.toObject( User.class );
                        buscarMensagem();
                    }
                } );



        //init();
    }

    public void buscarMensagem(){
        if(userAtual != null){
            String userRemetente = userAtual.getUuid();
            String userReceptor = user.getUuid();
            FirebaseFirestore.getInstance().collection( Constants.CONVERSAS )
                    .document(userRemetente)
                    .collection( userReceptor )
                    .orderBy( Constants.HORARIO_MSG, Query.Direction.ASCENDING )
                    .addSnapshotListener( new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                            List<DocumentChange> documentChanges = value.getDocumentChanges();
                            if(documentChanges != null){
                                for(DocumentChange doc : documentChanges){
                                    if(doc.getType() == DocumentChange.Type.ADDED){
                                        Mensagem mensagem = doc.getDocument().toObject( Mensagem.class );
                                        groupAdapter.add( new MensagemItem( mensagem ) );
                                        groupAdapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        }
                    } );
        }
    }


    public void enviarMessagem(){
        String conteudoMensagem = editTextMsgChat.getText().toString();
        editTextMsgChat.setText( null );

        //Usuario remetente
        final String userRemetente = FirebaseAuth.getInstance().getUid();
        //Usuario receptor
        final String userReceptor = user.getUuid();
        long horarioMsg = System.currentTimeMillis();

       final Mensagem mensagem = new Mensagem();
        mensagem.setConteudoMensagem( conteudoMensagem );
        mensagem.setHorarioMensagem( horarioMsg );
        mensagem.setRemetenteMensagem( userRemetente );
        mensagem.setReceptorMensagem( userReceptor );

        if(!mensagem.getConteudoMensagem().isEmpty()){
            FirebaseFirestore.getInstance().collection( Constants.CONVERSAS )
                    .document(userRemetente)
                    .collection( userReceptor )
                    .add( mensagem )
                    .addOnSuccessListener( new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Log.i("Teste", documentReference.getId());

                            Contato contato = new Contato();
                            contato.setUserID( userReceptor );
                            contato.setUsername( user.getUsername() );
                            contato.setPhotoURL( user.getUriImg() );
                            contato.setHorarioMsg( mensagem.getHorarioMensagem() );
                            contato.setUltitmaMensagem( mensagem.getConteudoMensagem() );

                            FirebaseFirestore.getInstance().collection( Constants.ULTIMAS_CONVERSAS )
                                    .document(userRemetente)
                                    .collection( Constants.CONTATOS )
                                    .document(userReceptor)
                                    .set( contato );
                        }
                    } )
                    .addOnFailureListener( new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.i("Teste", e.getMessage(), e);

                        }
                    } );

            FirebaseFirestore.getInstance().collection( Constants.CONVERSAS )
                    .document(userReceptor)
                    .collection( userRemetente )
                    .add( mensagem )
                    .addOnSuccessListener( new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Log.i("Teste", documentReference.getId());


                            Contato contato1 = new Contato();
                            contato1.setUserID( userReceptor );
                            contato1.setUsername( userAtual.getUsername() );
                            contato1.setPhotoURL( userAtual.getUriImg() );
                            contato1.setHorarioMsg( mensagem.getHorarioMensagem() );
                            contato1.setUltitmaMensagem( mensagem.getConteudoMensagem() );

                            FirebaseFirestore.getInstance().collection( Constants.ULTIMAS_CONVERSAS )
                                    .document(userReceptor)
                                    .collection( Constants.CONTATOS )
                                    .document(userRemetente)
                                    .set( contato1 );
                        }
                    } )
                    .addOnFailureListener( new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.i("Teste", e.getMessage(), e);

                        }
                    } );
        }
    }

    private class MensagemItem extends Item<ViewHolder> {
        private final Mensagem mensagem;

        private MensagemItem(Mensagem mensagem) {
            this.mensagem = mensagem;
        }


        @Override
        public void bind(@NonNull ViewHolder viewHolder, int position) {
            TextView textViewMensagemRem = viewHolder.itemView.findViewById( R.id.text_msg );
            ImageView imageViewRem = viewHolder.itemView.findViewById( R.id.imageView_chat );

            textViewMensagemRem.setText( mensagem.getConteudoMensagem() );
            Picasso.get()
                        .load(mensagem.getRemetenteMensagem().equals( FirebaseAuth.getInstance().getUid() )
                        ? userAtual.getUriImg()
                        : user.getUriImg())
                        .into( imageViewRem );
        }
        @Override
        public int getLayout() {
            return mensagem.getRemetenteMensagem().equals( FirebaseAuth.getInstance().getUid())
                ? R.layout.layout_msgs_recebida
                : R.layout.layout_remetente_msg;


        }
    }



}