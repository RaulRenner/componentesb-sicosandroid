package com.example.uolthasapp.models;

public class Contato {

    private String userID;
    private String username;
    private String ultitmaMensagem;
    private String photoURL;
    private long horarioMsg;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUltitmaMensagem() {
        return ultitmaMensagem;
    }

    public void setUltitmaMensagem(String ultitmaMensagem) {
        this.ultitmaMensagem = ultitmaMensagem;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public long getHorarioMsg() {
        return horarioMsg;
    }

    public void setHorarioMsg(long horarioMsg) {
        this.horarioMsg = horarioMsg;
    }
}
