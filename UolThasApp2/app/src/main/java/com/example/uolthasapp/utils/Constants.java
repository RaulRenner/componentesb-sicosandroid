package com.example.uolthasapp.utils;

public class Constants {
    public static String USERS = "/users";
    public static String CONTATOS = "contatos";
    public static String ULTIMAS_CONVERSAS = "/ultimas-conversas";
    public static String CONVERSAS = "/conversas";
    public static String HORARIO_MSG = "horarioMensagem";
    public static  String USER = "user";

}
