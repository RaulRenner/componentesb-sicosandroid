package com.example.uolthasapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.uolthasapp.models.Contato;
import com.example.uolthasapp.utils.Constants;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;
import com.xwray.groupie.GroupAdapter;
import com.xwray.groupie.Item;
import com.xwray.groupie.OnItemClickListener;
import com.xwray.groupie.ViewHolder;

import java.util.List;

public class ActivityMensagens extends AppCompatActivity {
    private FloatingActionButton floatingActionButton;
    private RecyclerView recyclerViewContatosMsgs;
    private GroupAdapter groupAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_mensagens );

        init();
        eventsClick();
        verificarAutenticacaoUser();
        buscarUltimaMensagem();
    }


    public void init(){
        floatingActionButton = (FloatingActionButton) findViewById( R.id.floatingActionButton );
        recyclerViewContatosMsgs = (RecyclerView) findViewById( R.id.recycleViewContatosMsgs );
        recyclerViewContatosMsgs.setLayoutManager( new LinearLayoutManager( this ) );
        groupAdapter = new GroupAdapter();
        recyclerViewContatosMsgs.setAdapter( groupAdapter );


    }

    public void eventsClick(){
        floatingActionButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( ActivityMensagens.this, ActivityContatos.class );
                startActivity( intent );
            }
        } );

   }

    public void buscarUltimaMensagem(){
        String userId = FirebaseAuth.getInstance().getUid();
        if(userId == null) return;
        FirebaseFirestore.getInstance().collection( Constants.ULTIMAS_CONVERSAS )
                .document(userId)
                .collection( Constants.CONTATOS )
                .addSnapshotListener( new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        List<DocumentChange> documentChangeList = value.getDocumentChanges();

                        if(documentChangeList != null){
                            for(DocumentChange doc : documentChangeList){
                                if(doc.getType() == DocumentChange.Type.ADDED){
                                    Contato contato =  doc.getDocument().toObject( Contato.class );
                                    groupAdapter.add( new ContatoItem( contato )  );
                                }
                            }
                        }
                    }
                } );
    }

    public void verificarAutenticacaoUser(){
        if(FirebaseAuth.getInstance().getUid() == null){
            Intent intent = new Intent( ActivityMensagens.this, ActivityLogin.class );

            intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivity( intent );
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate( R.menu.menu_options, menu );
        return super.onCreateOptionsMenu( menu );
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.id_logout:
                FirebaseAuth.getInstance().signOut();
                verificarAutenticacaoUser();
                break;
            case R.id.id_create_group:
//                Intent intent = new Intent( ActivityMensagens.this, ChatActivity.class );
//                startActivity( intent );
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private class ContatoItem extends Item<ViewHolder>{

        private final Contato contato;

        private ContatoItem(Contato contato) {
            this.contato = contato;
        }

        @Override
        public void bind(@NonNull ViewHolder viewHolder, int position) {
            TextView username = viewHolder.itemView.findViewById( R.id.textNomeContato );
            TextView ultimaMensagem  = viewHolder.itemView.findViewById( R.id.textViewUltimaMsg );
            ImageView imageView = viewHolder.itemView.findViewById( R.id.image_contato );

            username.setText( contato.getUsername() );
            ultimaMensagem.setText( contato.getUltitmaMensagem() );

            Picasso.get().load( contato.getPhotoURL() ).into( imageView );
        }

        @Override
        public int getLayout() {
            return R.layout.item_user_mensagens_layout;
        }
    }
}