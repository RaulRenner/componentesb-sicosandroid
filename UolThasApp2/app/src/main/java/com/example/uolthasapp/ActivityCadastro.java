package com.example.uolthasapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.uolthasapp.models.User;
import com.example.uolthasapp.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.UUID;

import javax.microedition.khronos.egl.EGLDisplay;

public class ActivityCadastro extends AppCompatActivity {
    private EditText editTextNome;
    private EditText editTextEmail;
    private EditText editTextSenha;
    private Button buttonVoltar;
    private Button buttonCadastrar;

    private Button buttonSelectPhoto;
    private ImageView imageViewCirclePerfil;
    private Uri uriImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        init();
        eventsClicks();
    }

    public void init(){
        editTextNome =  (EditText) findViewById( R.id.edt_nome);
        editTextEmail = (EditText) findViewById( R.id.edt_email );
        editTextSenha = (EditText) findViewById( R.id.edt_senha );
        buttonVoltar =  (Button) findViewById( R.id.btn_voltar );
        buttonCadastrar = (Button) findViewById( R.id.btn_cadastar );
        buttonSelectPhoto = (Button) findViewById( R.id.button_photo );
        imageViewCirclePerfil = (ImageView) findViewById( R.id.image_contato );

    }

    public void eventsClicks(){
        buttonVoltar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        } );

        buttonCadastrar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastrarUsuario();
            }
        } );

        buttonSelectPhoto.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType( "image/*" );
                startActivityForResult( intent, 0 );
            }
        } );
    }

    public void cadastrarUsuario(){
        String nome = editTextNome.getText().toString();
        String email = editTextEmail.getText().toString();
        String senha =  editTextSenha.getText().toString();
        Log.i("Teste", email);
        Log.i( "Teste", senha );
        if( nome == null || nome.isEmpty() || email == null || email.isEmpty() || senha == null  || senha.isEmpty()){
            Toast.makeText( ActivityCadastro.this, "Preencha todos os campos", Toast.LENGTH_LONG ).show();
            return;
        }
        FirebaseAuth.getInstance().createUserWithEmailAndPassword( email, senha )
                .addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Log.i("Teste2", task.getResult().getUser().getUid());
                            salavarUsuarioFirebase();

                        }
                    }
                } )
                .addOnFailureListener( new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.i("Teste2", e.getMessage());

                    }
                } );
        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult( requestCode, resultCode, data );

        if(requestCode == 0) {
            uriImage = data.getData();

            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap( getContentResolver(), uriImage );
                imageViewCirclePerfil.setImageDrawable( new BitmapDrawable( bitmap ) );
                buttonSelectPhoto.setAlpha( 0 );
            } catch (IOException e) {

            }
        }
    }

    public void salavarUsuarioFirebase(){
        String filename = UUID.randomUUID().toString();
        final StorageReference referenceImg = FirebaseStorage.getInstance().getReference("/images/" + filename);
        referenceImg.putFile( uriImage )
                .addOnSuccessListener( new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        referenceImg.getDownloadUrl().addOnSuccessListener( new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                Log.i( "Teste3", uri.toString() );

                                String id = FirebaseAuth.getInstance().getUid();
                                String username = editTextNome.getText().toString();
                                String urlImg = uri.toString();

                                User user = new User( id, username, urlImg );

                                FirebaseFirestore.getInstance().collection( Constants.USERS )
                                        .document(id)
                                        .set( user )
                                        .addOnSuccessListener( new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Intent intent = new Intent( ActivityCadastro.this, ActivityMensagens.class );
                                                intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );

                                                startActivity( intent );
                                            }
                                        } )
                                        .addOnFailureListener( new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.i("Teste", e.getMessage());
                                            }
                                        } );
                            }
                        } );
                    }
                } )
                .addOnFailureListener( new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e( "Teste4", e.getMessage(), e );
                    }
                } );
    }
}