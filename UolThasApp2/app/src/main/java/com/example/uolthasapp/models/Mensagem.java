package com.example.uolthasapp.models;

public class Mensagem {
    private String conteudoMensagem;
    private long horarioMensagem;
    private String remetenteMensagem;
    private String receptorMensagem;



//    public Mensagem(String conteudoMensagem, long horarioMensagem, String remetenteMensagem, String receptorMensagem) {
//        this.conteudoMensagem = conteudoMensagem;
//        this.horarioMensagem = horarioMensagem;
//        this.remetenteMensagem = remetenteMensagem;
//        this.receptorMensagem = receptorMensagem;
//    }

    public String getConteudoMensagem() {
        return conteudoMensagem;
    }

    public void setConteudoMensagem(String conteudoMensagem) {
        this.conteudoMensagem = conteudoMensagem;
    }

    public long getHorarioMensagem() {
        return horarioMensagem;
    }

    public void setHorarioMensagem(long horarioMensagem) {
        this.horarioMensagem = horarioMensagem;
    }

    public String getRemetenteMensagem() {
        return remetenteMensagem;
    }

    public void setRemetenteMensagem(String remetenteMensagem) {
        this.remetenteMensagem = remetenteMensagem;
    }

    public String getReceptorMensagem() {
        return receptorMensagem;
    }

    public void setReceptorMensagem(String receptorMensagem) {
        this.receptorMensagem = receptorMensagem;
    }
}
